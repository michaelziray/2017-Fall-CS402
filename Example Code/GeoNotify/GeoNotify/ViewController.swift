//
//  ViewController.swift
//  GeoNotify
//
//  Created by michael.ziray on 11/7/17.
//  Copyright © 2017 BSU. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UNUserNotificationCenterDelegate {

    var messageSubtitle:String = "We miss you!"
    var badgeNumber:NSNumber = 1
    
    let locationManager:CLLocationManager = CLLocationManager()
    
    let DEFAULTS_KEY:String = "KEY1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
            print("enabled!")
        }
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        let geoFenceRegion:CLCircularRegion = CLCircularRegion(center: CLLocationCoordinate2DMake(43.61871,  -116.214607), radius: 10.5, identifier: "Home")
        
        locationManager.startMonitoring(for: geoFenceRegion)
        
        UNUserNotificationCenter.current().delegate = self
        
        guard let userPreferenceValue:String = UserDefaults.standard.value(forKey: DEFAULTS_KEY) as? String else {
            UserDefaults.standard.set("here's the value", forKey: DEFAULTS_KEY)
            return
        }
        
//        if let userPreferenceValue:String = UserDefaults.standard.value(forKey: DEFAULTS_KEY) as? String {
//            print(userPreferenceValue) // scope is within this block only
//        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendNotification(_ sender: Any) {
        
        let content = UNMutableNotificationContent()
        content.title = "Notification from tap"
        content.subtitle = messageSubtitle
        content.body = "Don't forget to set a delegate"
        content.badge = 1
        
        content.categoryIdentifier = "actionCategory"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2,
                                                        repeats: false)
        
        let repeatAction = UNNotificationAction(identifier:"repeat",
                                                title:"Repeat",options:[])
        let changeAction = UNTextInputNotificationAction(identifier:
            "change", title: "Change Message", options: [])
        
        let category = UNNotificationCategory(identifier: "actionCategory",
                                              actions: [repeatAction, changeAction],
                                              intentIdentifiers: [], options: [])
        
        let requestIdentifier = "demoNotification"
        let request = UNNotificationRequest(identifier: requestIdentifier,
                                            content: content, trigger: trigger)
        UNUserNotificationCenter.current().setNotificationCategories( [category] )
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            // Handle error
            
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let content = UNMutableNotificationContent()
        content.title = "Did enter your geofence"
        content.subtitle = messageSubtitle
        content.body = "Welcome back"
        content.badge = badgeNumber
        
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1,
                                                        repeats: false)
        
        let requestIdentifier = "weeklyNotification"
        let request = UNNotificationRequest(identifier: requestIdentifier,
                                            content: content, trigger: trigger)
        
        
        //        UNUserNotificationCenter.current().setNotificationCategories( [category] )
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            
            if( error == nil ){
                // Handle error
            }
            else{
                // handle complete
            }
            
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        let content = UNMutableNotificationContent()
        content.title = "Did exit your geofence"
        content.subtitle = messageSubtitle
        content.body = "Goodbye!"
        content.badge = badgeNumber
        
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1,
                                                        repeats: false)
        
        let requestIdentifier = "weeklyNotification"
        let request = UNNotificationRequest(identifier: requestIdentifier,
                                            content: content, trigger: trigger)
        
        
        //        UNUserNotificationCenter.current().setNotificationCategories( [category] )
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            
            if( error == nil ){
                // Handle error
            }
            else{
                // handle complete
            }
            
        })
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case "repeat":
            self.sendNotification( self )
        case "change":
            let textResponse = response as! UNTextInputNotificationResponse
            messageSubtitle = textResponse.userText
            self.sendNotification( self )
        default:
            break
        }
        completionHandler()
    }
}

