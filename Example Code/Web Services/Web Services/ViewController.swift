//
//  ViewController.swift
//  Web Services
//
//  Created by michael.ziray on 9/19/17.
//  Copyright © 2017 Boise State. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Alamofire.request("https://s3-us-west-2.amazonaws.com/electronic-armory/buildings.json").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json:NSArray = (response.result.value as! NSArray) {
                print("JSON: \(json)") // serialized json response
                
                for jsonObject in json {
                    
                    let currentObject:Dictionary<String, Any> = jsonObject as! Dictionary
                    print("ID: \(currentObject["id"]!)")
                    print("Building Name: \(currentObject["name"]!)")
                    print("Building Description: \(currentObject["description"]!)")
                }
            }
            
//            if let json:NSArray = response.result.value as! NSArray {
//                print("JSON: \(json)") // serialized json response
//
//                for jsonObject in json{
//                    let currentBuilding:Dictionary<String, Any> = jsonObject as! Dictionary
//                    print(currentBuilding["name"]!)
//                }
            
            
            
        }
        print("About to load JSON")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

