//
//  ViewController.swift
//  Camera
//
//  Created by michael.ziray on 9/19/17.
//  Copyright © 2017 Boise State. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var didShow:Bool = false
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if( !didShow ){
            didShow = true
            let imagePicker:UIImagePickerController = UIImagePickerController()
            
            #if TARGET_IPHONE_SIMULATOR // This is backwards
                imagePicker.sourceType = .camera
            #else
                imagePicker.sourceType = .photoLibrary
            #endif
            
            imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        
        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("I hit the cancel button")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

