//: Playground - noun: a place where people can play

import Foundation

let constantString = "Constant"
//constantString += " here" // Can't mutate this object

let aMultilineString = """
        line one
    line two
        line three
    """

print(aMultilineString)
print("---------------------------------")

var strMutable = "Hello"
strMutable += " world"

var str:String = "Hello"
str += " world"
str = "hello world"

var someInt:Int = 10

print(str)
print(someInt)

str = "new hello"

var anotherVariable = str + " " + String(someInt)

someInt = someInt + 15

print(anotherVariable)
print("---------------------------------")

func functionName(){
    print("hello from a function")
}

functionName()
functionName()


func functionTwoName(stringToPrint:String){
    print("hello from a function")
    
    print("I'm done now!")
}

functionTwoName(stringToPrint: "A new string")
functionTwoName(stringToPrint: "Another string to print")

func functionWithNamelessParameter( _ stringToPrint:String){
    print("hello from a function")
    
    print("I'm done now!")
}
functionWithNamelessParameter("No name preceding this")
functionWithNamelessParameter("another one just for show")


print("---------------------------------")

func interestCalculator( principalAmount:Int, interestRate:Float ) -> Float{
    let newAmountOfMoney:Float = Float(principalAmount) * interestRate
    
    return newAmountOfMoney
}


func expontialGrowth(withRate:Float){
    var value:Float = 1
    while ( value < 1000 ){
        
        value = value * withRate
        print (value)
    }

}
//exponentialFunction(valueToSquare: <#T##Int#>)

expontialGrowth(withRate: 1.3)


class MobileDeveloper: NSObject{
    var personName:String = "No name "
    var experienceLevel:String = "CS402"
//    var salary:Float
   
    var salary: Float {
        get { return 100000.50 }
//        set { salary = newValue * 0.05 }
    }
    
    override init() {
        super.init()
//        salary = 100000.50
    }
    
    func printDeveloperDescription(){
        print("\(personName) has \(experienceLevel) of experience and makes $\(salary) per year")
    }
    
    override var description: String { return "The description is: \(personName)"}
    
    
}

let mike:MobileDeveloper = MobileDeveloper()

mike.personName = "Mike"
mike.experienceLevel = "10 years"
mike.printDeveloperDescription()
mike.description
print( mike )


