//
//  LoginViewController.swift
//  FirstApp
//
//  Created by michael.ziray on 8/29/17.
//  Copyright © 2017 Boise State. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var rememberMeSwitch: UISwitch!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("hello CS402!!")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        helloLabel.text = "hello CS402!!"
        
        print(rememberMeSwitch.isOn)
        
        usernameTextField.text = "my new email address"
    }
    
    @IBAction func switchDidChange(_ sender: UISwitch) {
        
        let isTheSwitchOn:Bool = sender.isOn
        helloLabel.text = "The value of the switch is: \(isTheSwitchOn)"
        print("The value of the switch is: \(isTheSwitchOn)")
        
        if isTheSwitchOn {
            helloLabel.text = "Something"
        }
        else{
            helloLabel.text = "Something else"
        }
    }
    
    
    @IBAction func loginTapped(_ sender: UIButton) {
        print("Button tapped!")
        
        print("Tag for button is: \(sender.tag)")
        
        let username = self.usernameTextField.text
        let password = passwordTextField.text
        
//        LoginController.attemptLogin( username, password )
        dismiss(animated: true, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        print("hello CS402!!")
    }


}

