//
//  MainViewController.swift
//  FirstApp
//
//  Created by michael.ziray on 9/5/17.
//  Copyright © 2017 Boise State. All rights reserved.
//

import UIKit

class MainViewController: UIViewController{
    
    var loginViewController:UIViewController?
    var isLogged:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("secondary view controller did load")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear( animated )
        
        if isLogged {
            print("We're logged in!! Do nothing")
        }
        else{
            let loginViewController:UIViewController = (storyboard?.instantiateViewController(withIdentifier: "LoginViewController"))!
            
            present(loginViewController, animated: true, completion: nil)
            isLogged = true
        }
    }
}










