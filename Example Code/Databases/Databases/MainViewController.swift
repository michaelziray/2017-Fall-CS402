//
//  ViewController.swift
//  Databases
//
//  Created by michael.ziray on 10/10/17.
//  Copyright © 2017 Boise State. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let fetchStudentsRequest:NSFetchRequest = Student.fetchRequest()
        
        
        let fetchPredicate:NSPredicate = NSPredicate(format: "studentName == %@", "Mike") //
        fetchStudentsRequest.predicate = fetchPredicate
        
        do{
            let fetchResults = try DatabaseController.persistentContainer.viewContext.fetch(fetchStudentsRequest)
            
            if( fetchResults.count > 0 ){
                for currentStudent in fetchResults {
                    print(currentStudent.studentName!)
                    
                    DatabaseController.managedObjectContext().delete(currentStudent)
                }
                
                
            }
            else{
                let newStudent:Student = Student(context:  DatabaseController.persistentContainer.viewContext)
                newStudent.studentName = "Mike Z"
                newStudent.year = "Super Senior first class"
            }
        }
        catch{
            print(exception.self)
        }
        
        
        
        DatabaseController.saveContext()
    }

    // MARK: - Here be dragons
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: -
}

